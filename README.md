# Micrus Demo Project

This is an example project that demonstrates the usage and features of the [Micrus framework](http://micrus.avris.it).

## Documentation

The documentation is available at **[docs.avris.it/micrus](https://docs.avris.it/micrus/)**

## Instalation

Install [Composer](https://getcomposer.org/download/) and run:

    $ composer create-project avris/micrus-demo my_new_project
    $ cd my_new_project
    $ bin/env
    $ bin/micrus db:schema:create
    $ bin/micrus db:fixtures
    $ yarn
    $ yarn server
    $ php -S localhost:8070 -t public/

Your website should be available under:

	http://localhost:8070
	
## Running tests

    $ vendor/bin/phpunit

## Copyright

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
