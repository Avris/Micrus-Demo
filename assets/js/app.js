const $ = require('jquery');
require('bootstrap/dist/js/bootstrap.bundle');
const MediumEditor = require('medium-editor');
require('select2');

require('../font-awesome/fontawesome-all');

require('./images.js');
require('./scroll.js');

$('.select2, .form-filter select').select2({
    width: '100%',
    theme: 'bootstrap'
});

editor = new MediumEditor('.editable', {
    toolbar: {
        buttons: ['bold', 'italic', 'underline', 'justifyFull', 'justifyCenter', 'anchor', 'h3', 'h4', 'quote']
    }
});
