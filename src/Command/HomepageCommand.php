<?php
namespace App\Command;

use Avris\Bag\Bag;
use Avris\Localisator\Order\LocaleOrderProviderInterface;
use Avris\Micrus\Bootstrap\AbstractApp;
use Avris\Micrus\Tool\DirectoryCleaner;
use Avris\Micrus\Tool\Test\WebClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Generates the homepage, https://micrus.avris.it
 *
 * Run: yarn build:prod && bin/micrus generate:homepage --env=prod --no-debug
 * Result in: var/home
 */
class HomepageCommand extends Command
{
    const ROBOTS = 'User-agent: *';

    const HTACCESS = <<<HTACCESS
DirectoryIndex en_GB.html

<IfModule mod_rewrite.c>
    RewriteEngine On

    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME}\.html -f
    RewriteRule ^(.+)$ $1.html [L]
</IfModule>

<IfModule !mod_rewrite.c>
    <IfModule mod_alias.c>
        RedirectMatch 302 ^([^.]+)$ $1.html
    </IfModule>
</IfModule>

ErrorDocument 404 /404.html
HTACCESS;

    /** @var WebClient */
    private $client;

    /** @var LocaleOrderProviderInterface */
    private $orderProvider;

    /** @var DirectoryCleaner */
    private $directoryCleaner;

    /** @var string */
    private $outputDir;

    /** @var string */
    private $publicDir;

    /** @var string */
    private $assetsDir;

    public function __construct(
        AbstractApp $app,
        LocaleOrderProviderInterface $orderProvider,
        DirectoryCleaner $directoryCleaner,
        Bag $configAssets,
        string $envProjectDir,
        string $envPublicDir
    ) {
        $this->client = new WebClient($app);
        $this->orderProvider = $orderProvider;
        $this->directoryCleaner = $directoryCleaner;
        $this->outputDir = $envProjectDir . '/var/home';
        $this->publicDir = $envPublicDir;
        $this->assetsDir = $configAssets->get('prefixAll', '');
        parent::__construct();
    }

    public function configure()
    {
        $this
            ->setName('generate:homepage')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->directoryCleaner->removeDir($this->outputDir);
        mkdir($this->outputDir, 0755, true);

        foreach ($this->orderProvider->getSupported() as $code => $name) {
            $output->writeln($name);
            $this->fetch($code);
        }

        $output->writeln('404');
        $this->fetch('404');

        $output->writeln('Copying assets');
        $this->copy(
            $this->publicDir . '/' . $this->assetsDir,
            $this->outputDir . '/' . $this->assetsDir
        );

        file_put_contents($this->outputDir . '/robots.txt', self::ROBOTS);
        file_put_contents($this->outputDir . '/.htaccess', self::HTACCESS);
    }

    private function fetch(string $url)
    {
        $html = $this->client->request('GET', '/' . $url . '?homepage=1')->html();

        file_put_contents($this->outputDir . '/' . $url . '.html', $html);
    }

    private function copy(string $source, string $dest)
    {
        if (is_link($source)) {
            return symlink(readlink($source), $dest);
        }

        if (is_file($source)) {
            return copy($source, $dest);
        }

        if (!is_dir($dest)) {
            mkdir($dest, 0755);
        }

        $dir = dir($source);
        while (false !== $entry = $dir->read()) {
            if ($entry === '.' || $entry === '..') {
                continue;
            }

            $this->copy($source . '/' . $entry, $dest . '/' . $entry);
        }

        $dir->close();

        return true;
    }
}
