<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DemoCommand extends Command
{
    public function configure()
    {
        $this
            ->setName('demo:demo')
            ->addArgument('filename', InputArgument::REQUIRED)
            ->addArgument('format', InputArgument::OPTIONAL, '', 'csv')
            ->addArgument('countFrom', InputArgument::OPTIONAL, '', 'now')
            ->addOption('force', 'f')
            ->addOption('comment', 'c', InputOption::VALUE_OPTIONAL);
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(
            sprintf(
                'Generating %s.%s (since %s)%s',
                $input->getArgument('filename'),
                $input->getArgument('format'),
                (new \DateTime($input->getArgument('countFrom')))->format('Y-m-d'),
                ($input->getOption('comment') ? ' with comment "' . $input->getOption('comment') . '"' : '')
            )
        );

        if ($input->getOption('force')) {
            $output->writeln('<info>Generated</info>');
        }
    }
}
