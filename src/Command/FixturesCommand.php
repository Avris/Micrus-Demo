<?php
namespace App\Command;

use App\Entity\Category;
use App\Entity\Post;
use App\Entity\User;
use Avris\Http\Request\UploadedFile;
use Avris\Micrus\Tool\FlashBag;
use Avris\Micrus\Tool\Security\CryptInterface;
use Avris\Micrus\Tool\Security\SecurityManager;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class FixturesCommand extends Command
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var CryptInterface */
    private $crypt;

    /** @var string */
    private $projectDir;

    const CATEGORIES = ['comedy','drama','tragedy','news','info','curiosity'];

    const POSTS = [
        'Dolores dolorum amet iste.' => ['drama', 'info', 'comedy'],
        'Voluptatem quisquam quibusdam sed.' => ['curiosity', 'comedy', 'news', 'tragedy'],
        'Ipsam sit veniam sed.' => ['comedy', 'news', 'curiosity'],
        'Dignissimos perferendis voluptatibus incidunt.' => ['comedy', 'info'],
        'Id et necessitatibus architecto.' => ['tragedy'],
        'Et id nisi qui.' => ['tragedy', 'info', 'drama'],
        'Ut iusto iusto.' => ['tragedy', 'news', 'comedy', 'info'],
        'Qui ducimus nihil laudantium nihil.' => ['info', 'curiosity'],
        'Natus ex dicta hic.' => ['curiosity'],
        'Est quae non quia.' => ['tragedy'],
        'Provident qui a voluptatem dignissimos.' => ['drama', 'tragedy', 'curiosity'],
        'Est nostrum et voluptas consequatur.' => ['tragedy', 'news', 'comedy', 'drama'],
    ];

    const FLASH_TYPES = [FlashBag::SUCCESS, FlashBag::DANGER, FlashBag::WARNING, FlashBag::INFO];

    public function __construct(EntityManagerInterface $em, CryptInterface $crypt, string $envProjectDir)
    {
        $this->em = $em;
        $this->crypt = $crypt;
        $this->projectDir = $envProjectDir;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('db:fixtures')
            ->setDescription('Load fixtures to the database')
            ->addOption('dont-truncate', 'd')
            ->addOption('force', 'f')
            ->addOption('local-image', 'l')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        $output->writeln('<info>Loading fixtures will truncate the database!</info>');

        $question = new ConfirmationQuestion('Are you sure you want to continue? (y/N) ', false);

        if (!$input->getOption('force') && !$helper->ask($input, $output, $question)) {
            return;
        }

        if (!$input->getOption('dont-truncate')) {
            $output->writeln('Truncating the database');
            $this->truncateDatabase();
        }

        $this->load($output, $input->getOption('local-image'));
    }

    protected function truncateDatabase()
    {
        $connection = $this->em->getConnection();
        $platform = $connection->getDatabasePlatform();

        $query = 'SET FOREIGN_KEY_CHECKS = 0;';
        foreach ($connection->getSchemaManager()->listTables() as $table) {
            $query .= $platform->getTruncateTableSQL($table->getName(), true) . ';';
        }
        $query .= 'SET FOREIGN_KEY_CHECKS = 1;';

        $connection->executeQuery($query);
    }

    protected function load(OutputInterface $output, bool $localImage)
    {
        $faker = Factory::create();
        $faker->seed(169);

        $output->writeln('Loading: users');

        $admin = new User();
        $admin->setUsername('admin');
        $admin->setEmail('admin@micrus.avris.it');
        $admin->setRole('ROLE_ADMIN');
        $admin->createAuthenticator(SecurityManager::AUTHENTICATOR_PASSWORD, $this->crypt->passwordHash('admin'));
        $this->em->persist($admin);

        $user = new User();
        $user->setUsername('user');
        $user->setEmail('user@micrus.avris.it');
        $user->setRole('ROLE_USER');
        $user->createAuthenticator(SecurityManager::AUTHENTICATOR_PASSWORD, $this->crypt->passwordHash('user'));
        $this->em->persist($user);

        $output->writeln('Loading: categories');
        $categories = [];
        foreach (self::CATEGORIES as $name) {
            $category = new Category();
            $category->setName($name);
            $this->em->persist($category);
            $categories[$name] = $category;
        }

        $output->writeln('Loading: posts');
        $i = 0;
        foreach (self::POSTS as $title => $postCategories) {
            $i++;
            $output->write("[$i/12]\r");
            $post = new Post([$admin, $user][$i % 2]);
            $post->setTitle($title);
            $content = '';
            for ($j = 0; $j < 5; $j++) {
                $content .= '<p style="text-align: justify">' . $faker->paragraph(5) . '</p>';
            }
            $post->setContent($content);
            $post->setPublishedAt($faker->dateTimeBetween('-' . (13 - $i) . ' months', '-' . (12 - $i) . ' months'));
            $post->setLanguage($faker->randomElement(['en_GB', 'en_US', 'pl', 'de', 'it']));
            $post->setNumber($faker->numberBetween(0, 5));
            $post->setUrl($faker->url);
            $post->setTypes($faker->randomElements(self::FLASH_TYPES, $faker->numberBetween(0, 4)));
            foreach ($postCategories as $category) {
                $post->addCategories($categories[$category]);
            }
            if ($faker->boolean(70)) {
                $post->setAttachment(new UploadedFile([
                    'name' => $i . '.jpg',
                    'type' => 'image/jpeg',
                    'tmp_name' => $this->getImagePath($faker, $localImage),
                    'error' => 0,
                    'size' => 0,
                ]));
            }
            $post->handleFileUpload($this->projectDir);
            $this->em->persist($post);
        }

        $this->em->flush();
        $this->em->clear();

        $output->writeln('Fixtures loaded');
    }

    private function getImagePath(Generator $faker, bool $local): string
    {
        if ($local) {
            copy($this->projectDir . '/assets/images/sample.jpg', $this->projectDir . '/var/tmp.jpg');
            return $this->projectDir . '/var/tmp.jpg';
        }

        return $faker->image();
    }
}
