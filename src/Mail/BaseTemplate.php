<?php
namespace App\Mail;

use Avris\Micrus\Mailer\Mail\Mail;
use Avris\Micrus\Mailer\MailTemplateInterface;

abstract class BaseTemplate implements MailTemplateInterface
{
    /** @var string */
    protected $projectDir;

    public function __construct(string $envProjectDir)
    {
        $this->projectDir = $envProjectDir;
    }

    public function getName(): string
    {
        if (!preg_match('#\\\\([^\\\\]*)Template$#', get_class($this), $matches)) {
            throw new \LogicException(sprintf('Invalid template classname: %s', get_class($this)));
        }

        return $matches[1];
    }

    public function extend(Mail $mail): Mail
    {
        $mail->embedImage('logo', $this->projectDir . '/assets/images/logo.png');
        $mail->embedImage('banner', $this->projectDir . '/assets/images/banner.png');

        return $mail;
    }
}
