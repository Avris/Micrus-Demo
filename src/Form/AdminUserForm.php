<?php
namespace App\Form;

use App\Entity\User;
use Avris\Forms\Widget as Widget;
use Avris\Forms\Form;

class AdminUserForm extends Form
{
    public function configure()
    {
        $this
            ->add('role', Widget\Choice::class, [
                'choices' => User::$roles,
            ])
        ;
    }
}
