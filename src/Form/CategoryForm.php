<?php
namespace App\Form;

use App\Entity\Post;
use Avris\Forms\Assert as Assert;
use Avris\Forms\Security\CsrfProvider;
use Avris\Forms\Widget as Widget;
use Avris\Forms\Form;
use Avris\Forms\WidgetFactory;
use Doctrine\ORM\EntityManagerInterface;

class CategoryForm extends Form
{
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(
        WidgetFactory $widgetFactory,
        CsrfProvider $csrfProvider,
        EntityManagerInterface $em
    ) {
        parent::__construct($widgetFactory, $csrfProvider);
        $this->em = $em;
    }

    public function configure()
    {
        $this
            ->add('name', Widget\Text::class, [], [
                new Assert\NotBlank(),
                new Assert\MaxLength(100),
            ])
            ->add('posts', Widget\Choice::class, [
                'choices' => $this->em->getRepository(Post::class)->findAll(),
                'keys' => function (Post $post) {
                    return $post->getId();
                },
                'multiple' => true,
                'class' => 'select2',
            ])
        ;
    }
}
