<?php
namespace App\Form;

use Avris\Forms\Assert as Assert;
use Avris\Forms\Security\CsrfProviderInterface;
use Avris\Forms\Widget as Widget;
use Avris\Forms\Form;
use Avris\Forms\WidgetFactory;
use Avris\Localisator\Order\LocaleOrderProviderInterface;

class MailForm extends Form
{
    /** @var LocaleOrderProviderInterface */
    private $orderProvider;

    public function __construct(
        LocaleOrderProviderInterface $orderProvider,
        WidgetFactory $widgetFactory,
        CsrfProviderInterface $csrfProvider
    ) {
        $this->orderProvider = $orderProvider;
        parent::__construct($widgetFactory, $csrfProvider);
    }

    public function configure()
    {
        $this
            ->add('email', Widget\Email::class, [
                'label' => l('entity:User.fields.email'),
            ], new Assert\NotBlank())
            ->add('locale', Widget\Choice::class, [
                'label' => l('menu.language'),
                'choices' => $this->orderProvider->getSupported()->all(),
                'default' => (string) $this->orderProvider->getCurrent(),
            ], new Assert\NotBlank())
        ;
    }
}
