<?php
namespace App\Form;

use Avris\Forms\Assert as Assert;
use Avris\Forms\Widget as Widget;
use Avris\Forms\Form;
use Avris\Micrus\Tool\FlashBag;

class FlashForm extends Form
{
    public function configure()
    {
        $this
            ->add('type', Widget\Choice::class, [
                'label' => '',
                'choices' => [
                    FlashBag::SUCCESS => FlashBag::SUCCESS,
                    FlashBag::WARNING => FlashBag::WARNING,
                    FlashBag::DANGER => FlashBag::DANGER,
                    FlashBag::INFO => FlashBag::INFO,
                ],
            ], new Assert\NotBlank)
            ->add('message', Widget\Text::class, [
                'label' => '',
                'attr' => [
                    'placeholder' => l('form.flash.message'),
                    'class' => 'input-large',
                ],
            ], new Assert\NotBlank)
        ;
    }
}
