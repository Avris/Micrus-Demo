<?php
namespace App\Form;

use App\Entity\Category;
use App\Entity\Dependency;
use App\Entity\User;
use Avris\Forms\Assert as Assert;
use Avris\Forms\ChoiceHelper;
use Avris\Forms\Form;
use Avris\Forms\Security\CsrfProvider;
use Avris\Forms\Widget as Widget;
use Avris\Forms\WidgetFactory;
use Avris\Micrus\Tool\FlashBag;
use Avris\Micrus\Tool\Security\SecurityManagerInterface;
use Doctrine\ORM\EntityManagerInterface;

class PostForm extends Form
{
    /** @var bool */
    private $isAdmin;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(
        WidgetFactory $widgetFactory,
        CsrfProvider $csrfProvider,
        SecurityManagerInterface $securityManager,
        EntityManagerInterface $em
    ) {
        $this->isAdmin = $securityManager->getUser()->isAdmin();
        parent::__construct($widgetFactory, $csrfProvider);
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->startSection(l('entity:Post.sections.general'))
            ->add('title', Widget\Text::class, [], [new Assert\NotBlank(), new Assert\MaxLength(100)])
            ->add('content', Widget\Textarea::class, [
                'class' => 'editable',
            ], new Assert\NotBlank())
        ;

        if ($this->isAdmin) {
            $this
                ->add('user', Widget\Choice::class, [
                    'choices' => $this->em->getRepository(User::class)->findAll(),
                    'keys' => function (User $user) {
                        return $user->getId();
                    },
                    'class' => 'select2'
                ], new Assert\NotBlank())
            ;
        }

        $this
            ->add('categories', Widget\Choice::class, [
                'choices' => $this->em->getRepository(Category::class)->findBy([], ['name' => 'asc']),
                'keys' => function (Category $category) {
                    return $category->getId();
                },
                'expanded' => false,
                'multiple' => true,
                'class' => 'select2',
            ], new Assert\NotBlank)
            ->add('language', Widget\Choice::class, [
                'choices' => ChoiceHelper::LANGUAGES,
                'add_empty' => true,
                'class' => 'select2',
            ], new Assert\NotBlank)
            ->add(
                'publishedAt',
                Widget\DateTime::class,
                [],
                [new Assert\NotBlank(), new Assert\MaxDate('now')]
            )
            ->startSection(l('entity:Post.sections.detail'))
            ->add('types', Widget\Choice::class, [
                'choices' => [
                    FlashBag::SUCCESS => '<span class="fas fa-check-circle"></span> Success',
                    FlashBag::WARNING => '<span class="fas fa-exclamation-triangle"></span> Warning',
                    FlashBag::DANGER => '<span class="fas fa-times-circle"></span> Danger',
                    FlashBag::INFO => '<span class="fas fa-exclamation-circle"></span> Info',
                ],
                'multiple' => true,
                'expanded' => true,
                'raw' => true,
            ])
            ->add(
                'number',
                Widget\Number::class,
                [],
                [new Assert\Max(5), new Assert\Step(0.5)]
            )
            ->add(
                'wholeNumber',
                Widget\Integer::class,
                [],
                [new Assert\Min(4), new Assert\Step(4)]
            )
            ->add('url', Widget\Url::class, [
                'helper' => 'Helper text displayed here',
                'attr' => [
                    'placeholder' => 'https://',
                ],
            ])
            ->add('attachment', Widget\File::class, [], [
                new Assert\File\File(),
                new Assert\File\MaxSize('5M'), // OR: 5 * 1024 * 1024
                new Assert\File\Image(),
                new Assert\File\MaxWidth(1000),
                //new Assert\File\Ratio(Assert\File\Ratio::VERTICAL),
                new Assert\File\Extension(['jpg', 'jpeg', 'png']),
            ])
            ->add('file', Widget\Custom::class, [
                'label' => '',
                'template' => 'Form/postFilePreview',
            ])
            ->add(
                'percent',
                Widget\TextAddon::class,
                ['after' => '%'],
                [new Assert\Number(), new Assert\Step(0.01)]
            )
            ->add('money', Widget\TextAddon::class, ['before' => '€'], [
                new Assert\Number(),
                new Assert\Min(0),
                new Assert\Step(0.01),
            ])
            ->add('notes', Widget\Multiple::class, [
                'widget' => Widget\Text::class,
                'element_asserts' => [
                    new Assert\NotBlank(),
                ]
            ], new Assert\MaxCount(3))
            ->add('dependencies', Widget\Multiple::class, [
                'widget' => DependencyForm::class,
                'element_prototype' => new Dependency(),
            ], [
                new Assert\UniqueField('option'),
                new Assert\MaxCount(2),
            ])
            ->add('mainDependency', DependencyForm::class, [
                'csrf' => false,
                'prototype' => new Dependency(),
            ])
        ;
    }
}
