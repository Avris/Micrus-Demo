<?php
namespace App\Form;

use App\Entity\User;
use Avris\Forms\Assert as Assert;
use Avris\Forms\Security\CsrfProviderInterface;
use Avris\Forms\Widget as Widget;
use Avris\Forms\Form;
use Avris\Forms\WidgetFactory;
use Avris\Micrus\Model\User\UserInterface;
use Avris\Micrus\Model\User\UserProviderInterface;
use Avris\Micrus\Tool\Security\CryptInterface;

class LoginForm extends Form
{
    /** @var CryptInterface */
    private $crypt;

    /** @var UserProviderInterface */
    private $userProvider;

    public function __construct(
        WidgetFactory $widgetFactory,
        CsrfProviderInterface $csrfProvider,
        CryptInterface $crypt,
        UserProviderInterface $userProvider
    ) {
        $this->crypt = $crypt;
        $this->userProvider = $userProvider;
        parent::__construct($widgetFactory, $csrfProvider);
    }

    public function configure()
    {
        $this
            ->add('username', Widget\Text::class, [], new Assert\NotBlank())
            ->add('password', Widget\Password::class, [], [
                new Assert\NotBlank(),
                new Assert\ValidCredentials(
                    [$this, 'buildObject'],
                    $this->crypt
                )
            ])
            ->add('rememberme', Widget\Checkbox::class, [
                'label' => '',
                'sublabel' => l('entity:User.fields.rememberMe'),
                'default' => true,
            ])
        ;
    }

    /**
     * @return User|UserInterface|null
     */
    public function buildObject($object = null)
    {
        return $this->userProvider->getUser($this->getData()['username'] ?? null);
    }

    public function shouldRememberThem():bool
    {
        return (bool) $this->getData()['rememberme'] ?? false;
    }

    public function getName(): string
    {
        return 'Login';
    }

    public function getObjectName(): ?string
    {
        return 'User';
    }
}
