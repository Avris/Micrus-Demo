<?php
namespace App\Form;

use App\Entity\User;
use Avris\Forms\Assert as Assert;
use Avris\Forms\Security\CsrfProviderInterface;
use Avris\Forms\Widget as Widget;
use Avris\Forms\Form;
use Avris\Forms\WidgetFactory;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class RegisterForm extends Form
{
    /** @var EntityRepository */
    private $repo;

    public function __construct(
        WidgetFactory $widgetFactory,
        CsrfProviderInterface $csrfProvider,
        EntityManagerInterface $em
    ) {
        parent::__construct($widgetFactory, $csrfProvider);
        $this->repo = $em->getRepository(User::class);
    }

    public function configure()
    {
        $this
            ->add('username', Widget\Text::class, [
                'placeholder' => l('entity:User.custom.usernameRegex'),
            ], [
                new Assert\NotBlank(),
                new Assert\Regexp('^[A-Za-z0-9_]+$', '', 'entity:User.custom.usernameRegex'),
                new Assert\MinLength(5),
                new Assert\MaxLength(25),
                new Assert\Callback(
                    function (string $value) {
                        return !$this->repo->findOneBy(['username' => $value]);
                    },
                    'entity:User.custom.usernameTaken'
                ),
            ])
            ->add('email', Widget\Email::class, [], [
                new Assert\NotBlank(),
                new Assert\Callback(
                    function (string $value) {
                        return !$this->repo->findOneBy(['email' => $value]);
                    },
                    'entity:User.custom.emailTaken'
                ),
            ])
            ->add('password', Widget\Password::class, [], [
                new Assert\NotBlank(),
                new Assert\MinLength(5),
                new Assert\Callback(
                    function () {
                        return $this->getData()['password'] === $this->getData()['passwordRepeat'];
                    },
                    'entity:User.custom.passwordMismatch'
                )
            ])
            ->add('passwordRepeat', Widget\Password::class, [], new Assert\NotBlank)
            ->add('agree', Widget\Checkbox::class, [
                'label' => '',
                'sublabel' => l('entity:User.custom.agreement')
            ], new Assert\NotBlank)
            //->add('captcha', \Avris\Micrus\ReCaptcha\Widget\ReCaptcha::class)
        ;
    }

    public function getPassword()
    {
        return $this->getData()['password'];
    }

    public function getName(): string
    {
        return 'Register';
    }
}
