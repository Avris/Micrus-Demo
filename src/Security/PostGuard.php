<?php
namespace App\Security;

use App\Entity\Post;
use App\Entity\User;
use Avris\Micrus\Model\User\UserInterface;
use Avris\Micrus\Tool\Security\GuardInterface;

class PostGuard implements GuardInterface
{
    public function supports(string $check, $object = null): bool
    {
        return $check === 'edit' && $object instanceof Post;
    }

    public function check(UserInterface $user, string $check, $object = null): bool
    {
        /** @var Post $object */

        if ($user->hasRole(User::ROLE_ADMIN)) {
            return true;
        }

        if (!$object) {
            return true;
        }

        return $object->getUser() === $user;
    }
}
