<?php
namespace App\Entity;

use Avris\Http\Request\UploadedFile;
use Avris\Forms\ChoiceHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table
 **/
class Post
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=36, options={"fixed" = true})
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $title = '';

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $content = '';

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $publishedAt;

    /**
     * @var string
     * @ORM\Column(type="string", length=7)
     */
    private $language = 'en';

    /**
     * @var float|null
     * @ORM\Column(type="float", nullable=true)
     */
    private $number;

    /**
     * @var integer|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wholeNumber;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $url;

    /**
     * @var float|null
     * @ORM\Column(type="float", nullable=true)
     */
    private $percent;

    /**
     * @var float|null
     * @ORM\Column(type="float", nullable=true)
     */
    private $money;

    /**
     * @var string[]
     * @ORM\Column(type="json_array")
     */
    private $types = [];

    /**
     * @var string[]
     * @ORM\Column(type="json_array")
     */
    private $notes = [];

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="posts")
     **/
    private $user;

    /**
     * @var Category[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="posts")
     * @ORM\JoinTable(name="category_post")
     **/
    private $categories;

    /**
     * @var UploadedFile
     */
    private $attachment;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $file;

    /**
     * @var Dependency[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Dependency", mappedBy="post", cascade={"persist", "remove"})
     */
    private $dependencies;

    /**
     * @var Dependency
     * @ORM\OneToOne(targetEntity="Dependency", cascade={"persist", "remove"})
     */
    private $mainDependency;

    public function __construct(User $user)
    {
        $this->categories = new ArrayCollection();
        $this->publishedAt = new \DateTime();
        $this->dependencies = new ArrayCollection();
        $this->mainDependency = new Dependency();
        $this->user = $user;
    }

    public function getIntro(): string
    {
        return mb_substr(strip_tags($this->content), 0, 300) . '...';
    }

    public function __toString(): string
    {
        return $this->title;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getPublishedAt(): \DateTime
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(\DateTime $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function getLanguageName(): string
    {
        return ChoiceHelper::LANGUAGES[$this->language];
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getNumber(): ?float
    {
        return $this->number;
    }

    public function setNumber($number): self
    {
        $this->number = $number ? (float) $number : null;

        return $this;
    }

    public function getWholeNumber(): ?int
    {
        return $this->wholeNumber;
    }

    public function setWholeNumber($wholeNumber): self
    {
        $this->wholeNumber = $wholeNumber ? (int) $wholeNumber : null;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getPercent(): ?float
    {
        return $this->percent;
    }

    public function setPercent($percent): self
    {
        $this->percent = $percent ? (float) $percent : null;

        return $this;
    }

    public function getMoney(): ?float
    {
        return $this->money;
    }

    public function setMoney($money): self
    {
        $this->money = $money ? (float) $money : null;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Category[]|ArrayCollection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    public function addCategories(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories->add($category);
        }

        return $this;
    }

    public function removeCategories(Category $category): self
    {
        $this->categories->removeElement($category);

        return $this;
    }

    public function getTypes(): array
    {
        return $this->types;
    }

    public function setTypes(array $types): self
    {
        $this->types = $types;

        return $this;
    }

    public function getNotes(): array
    {
        return $this->notes;
    }

    public function setNotes(array $notes): self
    {
        $this->notes = array_values($notes);

        return $this;
    }

    public function getAttachment(): ?UploadedFile
    {
        return $this->attachment;
    }

    public function setAttachment(?UploadedFile $attachment): self
    {
        $this->attachment = $attachment;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    // TODO in controller
    public function handleFileUpload(string $rootDir): self
    {
        if ($this->attachment) {
            $this->file = $this->attachment->moveToRandom($rootDir . '/var/pics/', $this->file);
        }

        return $this;
    }

    /**
     * @return Dependency[]|ArrayCollection
     */
    public function getDependencies()
    {
        return $this->dependencies;
    }

    public function addDependencies(Dependency $dependency): self
    {
        $dependency->setPost($this);
        $this->dependencies->add($dependency);

        return $this;
    }

    public function removeDependencies(Dependency $dependency): self
    {
        $this->dependencies->removeElement($dependency);
        $dependency->setPost(null);

        return $this;
    }

    public function getMainDependency(): Dependency
    {
        return $this->mainDependency;
    }

    public function setMainDependency(Dependency $mainDependency): self
    {
        $this->mainDependency = $mainDependency;

        return $this;
    }
}
