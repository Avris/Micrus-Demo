<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table
 **/
class Category
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=36, options={"fixed" = true})
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var Post[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="Post", mappedBy="categories")
     * @ORM\OrderBy({"publishedAt": "desc"})
     **/
    private $posts;

    public function __construct()
    {
        $this->posts = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Post[]|ArrayCollection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @return int
     */
    public function getPostsCount()
    {
        return $this->posts->count();
    }

    /**
     * @param Post $post
     * @return $this
     */
    public function addPosts(Post $post)
    {
        $this->posts->add($post);
        $post->addCategories($this);

        return $this;
    }

    /**
     * @param Post $post
     * @return $this
     */
    public function removePosts(Post $post)
    {
        $this->posts->removeElement($post);
        $post->removeCategories($this);

        return $this;
    }
}
