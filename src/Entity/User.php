<?php
namespace App\Entity;

use Avris\Micrus\Mailer\Mail\AddressInterface;
use Avris\Micrus\Model\User\AuthenticatorInterface;
use Avris\Micrus\Model\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table
 **/
class User implements UserInterface, AddressInterface
{
    const ROLE_USER = 'ROLE_USER';
    const ROLE_ADMIN = 'ROLE_ADMIN';

    public static $roles = [
        self::ROLE_USER => 'user',
        self::ROLE_ADMIN => 'admin',
    ];

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=36, options={"fixed" = true})
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $username;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $role;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var Authenticator[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Authenticator", mappedBy="user", cascade={"persist", "remove"})
     **/
    private $authenticators;

    /**
     * @var Post[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Post", mappedBy="user")
     **/
    private $posts;

    public function __construct()
    {
        $this->authenticators = new ArrayCollection();
        $this->role = self::ROLE_USER;
        $this->posts = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    public function getIdentifier()
    {
        return $this->getUsername();
    }

    public function getAuthenticators(string $type = null): iterable
    {
        return $this->authenticators->filter(function (Authenticator $auth) use ($type) {
            $typeValid = $type ? $auth->getType() === $type : true;
            return $auth->isValid() && $typeValid;
        });
    }

    public function createAuthenticator(
        string $type,
        string $payload,
        \DateTime $validUntil = null
    ): AuthenticatorInterface {
        $auth = new Authenticator();
        $auth->setType($type);
        $auth->setPayload($payload);
        $auth->setValidUntil($validUntil);
        $auth->setUser($this);

        $this->authenticators[] = $auth;

        return $auth;
    }

    public function getRoles(): iterable
    {
        return [$this->role];
    }

    public function hasRole(string $role): bool
    {
        return $this->role === $role;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getUsername();
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     * @return $this
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return string
     */
    public function getRoleName()
    {
        return self::$roles[$this->role];
    }

    public function isAdmin(): bool
    {
        return $this->role == self::ROLE_ADMIN;
    }

    public function __toString(): string
    {
        return $this->username;
    }

    /**
     * @return Post[]|ArrayCollection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @return int
     */
    public function getPostsCount()
    {
        return $this->posts->count();
    }

    /**
     * @param Post $post
     * @return $this
     */
    public function addPosts(Post $post)
    {
        $this->posts->add($post);
        $post->setUser($this);

        return $this;
    }

    /**
     * @param Post $post
     * @return $this
     */
    public function removePosts(Post $post)
    {
        $this->posts->removeElement($post);

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
