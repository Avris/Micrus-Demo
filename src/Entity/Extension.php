<?php
namespace App\Entity;

class Extension
{
    /** @var string */
    private $name;

    /** @var string|null */
    private $description;

    /** @var bool */
    private $micrusRepo;

    public function __construct(string $name, ?string $description = null, $micrusRepo = true)
    {
        $this->name = $name;
        $this->description = $description;
        $this->micrusRepo = $micrusRepo;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description ?? $this->name;
    }

    public function getRepo(): ?string
    {
        return sprintf('https://gitlab.com/Avris/%s%s', $this->micrusRepo ? 'Micrus-' : '', $this->name);
    }
}
