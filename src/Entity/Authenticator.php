<?php
namespace App\Entity;

use Avris\Micrus\Model\User\AuthenticatorInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table
 **/
class Authenticator implements AuthenticatorInterface
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=36, options={"fixed" = true})
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @var array|string
     * @ORM\Column(type="json_array")
     */
    private $payload;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $validUntil;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="authenticators")
     **/
    private $user;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Authenticator
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return array|string
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @param array|string $payload
     * @return Authenticator
     */
    public function setPayload($payload)
    {
        $this->payload = $payload;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getValidUntil()
    {
        return $this->validUntil;
    }

    /**
     * @param \DateTime $validUntil
     * @return Authenticator
     */
    public function setValidUntil($validUntil)
    {
        $this->validUntil = $validUntil;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Authenticator
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return $this->getValidUntil() === null || $this->getValidUntil() >= new \DateTime();
    }

    /**
     * @return Authenticator
     */
    public function invalidate()
    {
        return $this->setValidUntil(new \DateTime('0000-00-00'));
    }
}
