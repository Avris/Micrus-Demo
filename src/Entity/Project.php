<?php
namespace App\Entity;

class Project
{
    /** @var string */
    private $name;

    /** @var string|null */
    private $website;

    /** @var string|null */
    private $repo;

    public function __construct(string $name, ?string $website = null, ?string $repo = null)
    {
        $this->name = $name;
        $this->website = $website;
        $this->repo = $repo;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getWebsite(): string
    {
        return $this->website ?: sprintf('https://%s.avris.it', strtolower($this->name));
    }

    public function getRepo(): string
    {
        return sprintf('https://gitlab.com/Avris/%s', $this->repo ?: $this->name);
    }
}
