<?php
namespace App\Controller;

use App\Entity\Extension;
use App\Entity\Project;
use App\Form\FlashForm;
use App\Form\MailForm;
use App\Entity\User;
use App\Mail\TestTemplate;
use App\Service\FileDownloader;
use Avris\Micrus\Annotations\Annotation as M;
use Avris\Micrus\Controller\Controller;
use Avris\Http\Response\FileResponse;
use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Mailer\Mail\Address;
use Avris\Micrus\Mailer\MailBuilder;
use Avris\Micrus\Mailer\Mailer;
use Avris\Micrus\Tool\FlashBag;

class HomeController extends Controller
{
    /**
     * @M\Route("/")
     */
    public function homeAction()
    {
        return $this->render([
            'projects' => [
                new Project('Dibsy'),
                new Project('Generator'),
                new Project('Avris Flags', 'https://flags.avris.it/', 'Flags-Home'),
                new Project('Avi'),
                new Project('Volatile'),
                new Project('Cropper'),
                new Project('Can I go home now', 'https://canigohome.avris.it/', 'GoHome'),
                new Project('QC', null, 'QC-Home'),
                new Project('Futurus', 'https://futurysci.avris.it/', 'Futurus-home'),
                new Project('Clavis'),
            ],
            'extensions' => [
                new Extension('Forms', null, false),
                new Extension('Localisator', null, false),
                new Extension('Twig', 'Twig module'),
                new Extension('Doctrine', 'Doctrine module'),
                new Extension('Mailer'),
                new Extension('Crud', 'CRUD generator'),
                new Extension('Imagine', 'Imagine module'),
                new Extension('Social', 'Social module'),
                new Extension('Annotations', 'Annotations module'),
                new Extension('GA', 'Google Analytics module'),
                new Extension('ReCaptcha', 'ReCaptcha widget'),
            ],
        ]);
    }

    /**
     * @M\Route("/hello/{uuid:id}", name="helloUser")
     */
    public function helloUserAction(User $user)
    {
        return $this->render([
            'name' => $user->getUsername(),
        ], 'Home/hello');
    }

    /**
     * @M\Route("/hello/{name=}", name="helloStranger")
     */
    public function helloStrangerAction($name = null)
    {
        return $this->render([
            'name' => $name ?: l('hello.stranger'),
        ], 'Home/hello');
    }

    /**
     * @M\Route("/extensions", name="extensions")
     */
    public function extensionsAction()
    {
        return $this->render();
    }

    /**
     * @M\Route("/flash", name="flash")
     */
    public function flashAction(RequestInterface $request)
    {
        $form = $this->form(FlashForm::class, null, $request);

        if ($data = $this->handleForm($form)) {
            $this->addFlash($data->type, $data->message);

            return $this->redirect($this->generateUrl('flash'));
        }

        return $this->render(['form' => $form]);
    }

    /**
     * @M\Route("/cookies", name="cookies")
     */
    public function cookiesAction(RequestInterface $request)
    {
        $count = $request->getCookies()->get('count', 0);

        $this->setCookie('count', $count + 1);

        return $this->render(['count' => $count]);
    }

    /**
     * @M\Route("/mail", name="mail")
     */
    public function mailAction(RequestInterface $request, MailBuilder $mailBuilder, Mailer $mailer)
    {
        $form = $this->form(MailForm::class, null, $request);
        $data = $this->handleForm($form);

        if (!$data) {
            return $this->render([
                'form' => $form,
            ]);
        }

        $mail = $mailBuilder->build(
            $this->get(TestTemplate::class),
            $data->locale,
            [
                'user' => 'Anonymous User',
                'foo' => 'bar',
            ]
        );
        $mail->addTo(new Address($data->email));

        if ($request->getData()->has('send')) {
            $result = $mailer->spool($mail);

            $this->addFlash(
                $result ? FlashBag::SUCCESS : FlashBag::DANGER,
                l(
                    'email.' . ($result ? 'spooled' : 'failed'),
                    ['email' => $data->email]
                ),
                false
            );
        }

        return $this->render([
            'form' => $form,
            'mail' => $mail,
        ]);
    }

    /**
     * Demonstration of JSON responses and of retrieving services that are created in factory mode
     *
     * @M\Route("/factory", name="factory")
     */
    public function factoryAction()
    {
        return $this->renderJson([
            1 => $this->get('countedProduct')->getValue(),        //should be 1
            2 => $this->get('countedProduct')->getValue(),        //should be 1
            3 => $this->get('countedProduct')->getValue(),        //should be 1
            4 => $this->get('countedProductFactory')->getValue(), //should be 2
            5 => $this->get('countedProductFactory')->getValue(), //should be 3
            6 => $this->get('countedProductFactory')->getValue(), //should be 4
            7 => $this->get('countedProduct')->getValue(),        //should be 1
            8 => $this->get('countedProduct')->getValue(),        //should be 1
            9 => $this->get('countedProduct')->getValue(),        //should be 1
        ]);
    }

    /**
     * @M\Route("/files", name="files")
     */
    public function filesAction()
    {
        return $this->render();
    }

    /**
     * @M\Route("/files/logo/{download=}", name="filesLogo")
     */
    public function filesLogoAction($download = false)
    {
        return new FileResponse($this->getProjectDir() . '/assets/images/logo.png', (bool) $download);
    }

    /**
     * @M\Route("/files/fetch", name="filesFetch")
     */
    public function filesLogoDownloadAction(FileDownloader $downloader)
    {
        $file = $downloader->download(
            'https://www.google.de/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png'
        );

        return new FileResponse($file->getTmpName());
    }
}
