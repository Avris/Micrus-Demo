<?php
namespace App\Controller;

use App\Form\LoginForm;
use App\Form\RegisterForm;
use App\Entity\User;
use Avris\Micrus\Annotations\Annotation as M;
use Avris\Micrus\Controller\Controller;
use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Micrus\Tool\Security\CryptInterface;
use Avris\Micrus\Tool\Security\SecurityManager;
use Avris\Micrus\Tool\Security\SecurityManagerInterface;
use Doctrine\ORM\EntityManagerInterface;

class UserController extends Controller
{
    /**
     * @M\Route("/login", name="login")
     */
    public function loginAction(
        EntityManagerInterface $em,
        SecurityManagerInterface $sm,
        RequestInterface $request,
        RouterInterface $router,
        CryptInterface $crypt
    ) {
        if ($this->getUser()) {
            return $this->redirect($this->generateUrl('account'));
        }

        /** @var LoginForm $loginForm */
        $loginForm = $this->form(LoginForm::class, null, $request);

        /** @var User $user */
        if ($user = $this->handleForm($loginForm)) {
            $sm->login($user, $loginForm->shouldRememberThem() ? $this->responseCookies : null);
            $em->persist($user);
            $em->flush();

            $url = $request->getQuery()->get('url');

            return $this->redirect($url ? $router->prependToUrl($url) : $this->generateUrl('account'));
        }

        /** @var RegisterForm $registerForm */
        $registerForm = $this->form(RegisterForm::class, new User(), $request);

        /** @var User $user */
        if ($user = $this->handleForm($registerForm)) {
            $user->createAuthenticator(
                SecurityManager::AUTHENTICATOR_PASSWORD,
                $crypt->passwordHash($registerForm->getPassword())
            );
            $sm->login($user);
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('account');
        }

        return $this->render([
            'loginForm' => $loginForm,
            'registerForm' => $registerForm,
        ]);
    }

    /**
     * @M\Route("/account", name="account")
     * @M\Secure
     */
    public function accountAction()
    {
        return $this->render();
    }
}
