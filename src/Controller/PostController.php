<?php
namespace App\Controller;

use App\Form\PostForm;
use App\Entity\Category;
use App\Entity\Post;
use Avris\Micrus\Annotations\Annotation as M;
use Avris\Micrus\Controller\Controller;
use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Tool\FlashBag;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @M\Route("/post")
 */
class PostController extends Controller
{
    /**
     * @M\Route("/list")
     */
    public function listAction(EntityManagerInterface $em)
    {
        return $this->render([
            'posts' => $em->getRepository(Post::class)->findBy([], ['publishedAt' => 'DESC']),
        ]);
    }

    /**
     * @M\Route("/{uuid:id}/read")
     */
    public function readAction(Post $post)
    {
        return $this->render([
            'posts' => [$post],
            'long' => true,
        ], 'Post/list');
    }

    /**
     * @M\Route("/category/{name}")
     */
    public function categoryAction(Category $category)
    {
        return $this->render(['posts' => $category->getPosts()], 'Post/list');
    }

    /**
     * @M\Route("/add", name="postAdd")
     * @M\Route("/{uuid:id}/edit", name="postEdit")
     * @M\Secure(check="edit", object="$post")
     */
    public function formAction(EntityManagerInterface $em, RequestInterface $request, Post $post = null)
    {
        if (!$post) {
            $post = new Post($this->getUser());
        }

        $form = $this->form(PostForm::class, $post, $request);

        if ($this->handleForm($form, $post)) {
            $post->handleFileUpload($this->getProjectDir());
            $em->persist($post);
            $em->flush();

            $this->addFlash(FlashBag::SUCCESS, l('entity:Post.actions.saveSuccess', ['title' => $post->getTitle()]));

            return $this->redirectToRoute('postRead', ['id' => $post->getId()]);
        }

        return $this->render([
            'form' => $form,
        ], 'Post/form');
    }

    /**
     * @M\Route("/{uuid:id}/delete")
     */
    public function deleteAction(EntityManagerInterface $em, RequestInterface $request, Post $post)
    {
        if ($request->isPost()) {
            $em->remove($post);
            $em->flush();
            $this->addFlash(FlashBag::SUCCESS, l('entity:Post.actions.deleteSuccess', ['title' => $post->getTitle()]));

            return $this->redirectToRoute('postList');
        }

        return $this->render(['post' => $post]);
    }
}
