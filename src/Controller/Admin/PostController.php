<?php
namespace App\Controller\Admin;

use App\Entity\Post;
use App\Entity\User;
use Avris\Micrus\Crud\Annotation\Crud;
use Avris\Micrus\Crud\Annotation\CrudMetric;
use Avris\Micrus\Crud\Config\ExportConfig;
use Avris\Micrus\Crud\Config\GeneralConfig;
use Avris\Micrus\Crud\Config\ListConfig;
use Avris\Micrus\Crud\Config\ShowConfig;
use Avris\Micrus\Crud\Controller\CrudController;
use Avris\Forms\ChoiceHelper;

/**
 * @Crud(
 *     "App\Entity\Post",
 *     form="App\Form\PostForm",
 *     icon="fas fa-file",
 *     perPage=5,
 *     metrics={
 *        "all": @CrudMetric("Avris\Micrus\Crud\Metric\CountMetric"),
 *        "lastWeek": @CrudMetric("Avris\Micrus\Crud\Metric\CountMetric", filters={"publishedAt":">@ -1 week"})
 *     }
 * )
 */
class PostController extends CrudController
{
    protected function configureList(ListConfig $config)
    {
        $config
            ->add('image', false, false, false, 'Crud/imageMini', '')
            ->add('title', true)
            ->add('user', 'User', true, User::class)
            ->add('publishedAt')
            ->add('categories', 'Category')
            ->add('language', false, false, ChoiceHelper::LANGUAGES)
        ;
    }

    protected function configureExport(ExportConfig $config)
    {
        $config
            ->add('title')
            ->add('user')
            ->add('publishedAt')
            ->add('categories')
            ->add('language')
        ;
    }

    protected function configureShow(ShowConfig $config)
    {
        $config
            ->startSection(l('entity:Post.sections.general'))
                ->add('title')
                ->add('content', false, true)
                ->add('user', 'User')
                ->add('categories', 'Category')
                ->add('languageName')
                ->add('publishedAt')
                ->add('file', false, false, 'Crud/image')
            ->endSection()
            ->startSection(l('entity:Post.sections.detail'))
                ->add('types')
                ->add('number')
                ->add('url', false, false, 'Crud/url')
            ->endSection()
        ;
    }

    protected function configureGeneral(GeneralConfig $config)
    {
        $config
            ->addAction('Crud/postRead')
        ;
    }

    protected function create()
    {
        return new Post($this->getUser());
    }
}
