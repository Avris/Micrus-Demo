<?php
namespace App\Controller\Admin;

use App\Form\CategoryForm;
use Avris\Micrus\Crud\Annotation\Crud;
use Avris\Micrus\Crud\Annotation\CrudMetric;
use Avris\Micrus\Crud\Config\ExportConfig;
use Avris\Micrus\Crud\Config\ListConfig;
use Avris\Micrus\Crud\Config\ShowConfig;
use Avris\Micrus\Crud\Controller\CrudController;

/**
 * @Crud(
 *     "App\Entity\Category",
 *     form="App\Form\CategoryForm",
 *     icon="fas fa-folder",
 *     metrics={
 *        "all": @CrudMetric("Avris\Micrus\Crud\Metric\CountMetric")
 *     }
 * )
 */
class CategoryController extends CrudController
{
    protected function configureList(ListConfig $config)
    {
        $config
            ->add('name', true)
            ->add('postsCount', false, false, false)
        ;
    }

    protected function configureExport(ExportConfig $config)
    {
        $config
            ->add('name')
            ->add('postsCount')
        ;
    }

    protected function configureShow(ShowConfig $config)
    {
        $config
            ->add('id')
            ->add('name')
            ->add('posts', 'Post')
        ;
    }
}
