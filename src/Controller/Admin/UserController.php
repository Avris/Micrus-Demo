<?php
namespace App\Controller\Admin;

use App\Form\AdminUserForm;
use App\Entity\User;
use App\Mail\TestTemplate;
use Avris\Micrus\Crud\Annotation\Crud;
use Avris\Micrus\Crud\Annotation\CrudMetric;
use Avris\Micrus\Crud\Config\ExportConfig;
use Avris\Micrus\Crud\Config\ListConfig;
use Avris\Micrus\Crud\Config\ShowConfig;
use Avris\Micrus\Crud\Controller\CrudController;
use Avris\Micrus\Mailer\MailBuilder;
use Avris\Micrus\Mailer\Mailer;
use Avris\Micrus\Tool\FlashBag;

/**
 * @Crud(
 *     "App\Entity\User",
 *     form="App\Form\AdminUserForm",
 *     icon="fas fa-users",
 *     metrics={
 *        "all": @CrudMetric("Avris\Micrus\Crud\Metric\CountMetric"),
 *        "lastWeek": @CrudMetric("Avris\Micrus\Crud\Metric\CountMetric", filters={"createdAt":">@ -1 week"}),
 *        "postsAvg": @CrudMetric("Avris\Micrus\Crud\Metric\AvgCountMetric", relation="posts"),
 *     },
 *     disableRoutes={"new"},
 *     addRoutes={"email": "/{__restr__:id}/email"}
 * )
 */
class UserController extends CrudController
{
    protected function configureList(ListConfig $config)
    {
        $config
            ->add('username', true)
            ->add('email', false, false)
            ->add('postsCount', false, false, false)
            ->add('roleName')
            ->add('createdAt')
            ->addAction('Crud/testEmail')
            ->addAction('Crud/impersonate')
        ;
    }

    protected function configureExport(ExportConfig $config)
    {
        $config
            ->add('username')
            ->add('email')
            ->add('postsCount')
            ->add('roleName')
        ;
    }

    protected function configureShow(ShowConfig $config)
    {
        $config
            ->add('username')
            ->add('email')
            ->add('roleName')
            ->add('posts', 'Post')
            ->add('createdAt')
        ;
    }

    public function emailAction(User $user, MailBuilder $mailBuilder, Mailer $mailer)
    {
        $mail = $mailBuilder->build(
            $this->get(TestTemplate::class),
            'en', // TODO $user->getLocale()
            [
                'user' => $user->getName(),
                'foo' => 'bar',
            ]
        )->addTo($user);

        $result = $mailer->spool($mail);

        $this->addFlash(
            $result ? FlashBag::SUCCESS : FlashBag::DANGER,
            l(
                'email.' . ($result ? 'spooled' : 'failed'),
                ['email' => $user->getEmail()]
            ),
            false
        );

        return $this->redirectToRoute('admin_User_list');
    }
}
