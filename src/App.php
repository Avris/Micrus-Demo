<?php
namespace App;

use Avris\Micrus\Bootstrap\AbstractApp;
use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Bootstrap\ModuleTrait;
use Avris\Micrus\Tool\Cache\Cacher;
use Avris\Micrus\Tool\Cache\CacherInterface;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

final class App extends AbstractApp implements ModuleInterface
{
    use ModuleTrait;

    protected function registerModules(): iterable
    {
        yield new \Avris\Micrus\Twig\TwigModule;
        yield new \Avris\Micrus\Doctrine\DoctrineModule;
        yield new \Avris\Micrus\Annotations\AnnotationsModule;
        yield new \Avris\Micrus\Crud\CrudModule;
        yield new \Avris\Micrus\Imagine\ImagineModule;
        yield new \Avris\Micrus\Mailer\MailerModule;
        yield new \Avris\Micrus\GoogleAnalytics\GoogleAnalyticsModule;
        yield new \Avris\Localisator\LocalisatorModule;
        yield new \Avris\Polonisator\PolonisatorModule();
        yield new \Avris\Forms\FormsModule;
        //yield new \Avris\Micrus\ReCaptcha\ReCaptchaModule;
        yield $this;
    }

    protected function buildProjectDir(): string
    {
        return dirname(__DIR__);
    }

    protected function buildCacher(): CacherInterface
    {
        if ($this->configBag->isDebug()) {
            return new Cacher(new ArrayAdapter(), true);
        }

        return new Cacher(new FilesystemAdapter('app', 0, $this->configBag->getDir(self::CACHE_DIR)), false);
    }
}
