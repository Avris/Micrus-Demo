<?php
namespace App\Service;

use Avris\Dispatcher\EventSubscriberInterface;
use Avris\Http\Response\JsonResponse;
use Avris\Micrus\Exception\Handler\ErrorEvent;

class ErrorTemplate implements EventSubscriberInterface
{
    /** @var bool */
    private $debug;

    public function __construct(bool $envAppDebug)
    {
        $this->debug = $envAppDebug;
    }

    public function onError(ErrorEvent $event)
    {
        if ($this->debug) {
            return;
        }

        $e = $event->getException();
        $code = in_array($e->getCode(), [403, 404, 500]) ? $e->getCode() : 500;

        if ($code == 403) {
            $event->setResponse(new JsonResponse(['error' => 'Unauthorized access'], $code));
        }
    }

    public function getSubscribedEvents(): iterable
    {
        yield 'error' => [$this, 'onError'];
    }
}
