<?php
namespace App\Service;

use Twig\Extension\AbstractExtension;

class WaveTwigExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('wave', function (string $text) {
                for ($i = 0, $waved = ''; $i < strlen($text); $i++) {
                    $waved .= $i % 2 ? strtoupper($text[$i]) : strtolower($text[$i]);
                }

                return $waved;
            }),
        ];
    }
}
