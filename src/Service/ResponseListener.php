<?php
namespace App\Service;

use Avris\Dispatcher\EventSubscriberInterface;
use Avris\Micrus\Controller\ResponseEvent;
use Psr\Log\LoggerInterface;

class ResponseListener implements EventSubscriberInterface
{
    /** @var LoggerInterface $logger */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function getSubscribedEvents(): iterable
    {
        yield 'response' => function (ResponseEvent $event) {
            $this->logger->log('info', 'Response length: ' . strlen($event->getResponse()->getContent()));
        };
    }
}
