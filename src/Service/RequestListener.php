<?php
namespace App\Service;

use Avris\Dispatcher\EventSubscriberInterface;
use Avris\Http\Response\JsonResponse;
use Avris\Micrus\Controller\RequestEvent;

class RequestListener implements EventSubscriberInterface
{
    public function getSubscribedEvents(): iterable
    {
        yield 'request' => function (RequestEvent $event) {
            if ($get = $event->getRequest()->getQuery()->get('json')) {
                $event->setResponse(new JsonResponse(['get' => $get]));
            }
        };
    }
}
