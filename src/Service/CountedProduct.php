<?php
namespace App\Service;

class CountedProduct
{
    /** @var int */
    protected static $count = 0;

    protected $value;

    public function __construct()
    {
        $this->value = ++static::$count;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    public static function reset()
    {
        static::$count = 0;
    }
}
