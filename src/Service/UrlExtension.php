<?php
namespace App\Service;

use Twig\Extension\AbstractExtension;

class UrlExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('clearUrl', function (string $url) {
                $url = trim($url);
                $url = str_replace(['http://', 'https://', 'http://www.', 'https://www.'], '', $url);
                if (strpos($url, '?')) {
                    $url = substr($url, 0, strpos($url, '?'));
                }

                return trim($url, '/');
            }),
        ];
    }
}
