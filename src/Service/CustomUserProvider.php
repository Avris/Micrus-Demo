<?php
namespace App\Service;

use App\Entity\User;
use Avris\Micrus\Model\User\UserInterface;
use Avris\Micrus\Model\User\UserProviderInterface;
use Doctrine\ORM\EntityManagerInterface;

final class CustomUserProvider implements UserProviderInterface
{
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getUser(string $identifier): ?UserInterface
    {
        $column = strpos($identifier, '@') === false ? 'username' : 'email';

        return $this->em->getRepository(User::class)->findOneBy([$column => $identifier]);
    }
}
