<?php
namespace App\Command;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class DemoCommandTest extends TestCase
{
    const COMMAND_NAME = 'demo:demo';

    /** @var string */
    private $today;

    /** @var CommandTester */
    private $commandTester;

    protected function setUp()
    {
        $this->today = (new \DateTime())->format('Y-m-d');

        $app = new Application();
        $app->add(new DemoCommand());

        $this->commandTester = new CommandTester($app->find(self::COMMAND_NAME));
    }

    /**
     * @expectedException \Symfony\Component\Console\Exception\RuntimeException
     * @expectedExceptionMessage Not enough arguments (missing: "filename").
     */
    public function testExecuteMissingFilename()
    {
        $this->commandTester->execute([
            'command' => self::COMMAND_NAME,
        ]);
    }

    public function testExecuteDefaultExtensionAndDate()
    {
        $this->commandTester->execute([
            'command' => self::COMMAND_NAME,
            'filename' => 'test',
        ]);

        $this->assertContains(
            'Generating test.csv (since '.$this->today.')',
            $this->commandTester->getDisplay()
        );
    }

    public function testExecuteDefaultDate()
    {
        $this->commandTester->execute([
            'command' => self::COMMAND_NAME,
            'filename' => 'test',
            'format' => 'txt',
        ]);

        $this->assertContains(
            'Generating test.txt (since '.$this->today.')',
            $this->commandTester->getDisplay()
        );
    }

    public function testExecuteAllGiven()
    {
        $this->commandTester->execute([
            'command' => self::COMMAND_NAME,
            'filename' => 'test',
            'format' => 'txt',
            'countFrom' => '2014-01-01'
        ]);

        $this->assertContains(
            'Generating test.txt (since 2014-01-01)',
            $this->commandTester->getDisplay()
        );
    }

    public function testExecuteWithCommend()
    {
        $this->commandTester->execute([
            'command' => self::COMMAND_NAME,
            'filename' => 'test',
            'format' => 'txt',
            '--comment' => 'komentarz',
        ]);

        $this->assertContains(
            'Generating test.txt (since '.$this->today.') with comment "komentarz"',
            $this->commandTester->getDisplay()
        );
    }

    public function testExecuteForced()
    {
        $this->commandTester->execute([
            'command' => self::COMMAND_NAME,
            'filename' => 'test',
            'format' => 'txt',
            '-f' => true,
        ]);

        $this->assertContains(
            'Generating test.txt (since '.$this->today.')',
            $this->commandTester->getDisplay()
        );

        $this->assertContains(
            'Generated',
            $this->commandTester->getDisplay()
        );
    }
}
