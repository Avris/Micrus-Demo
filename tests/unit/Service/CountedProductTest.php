<?php
namespace App\Service;

use PHPUnit\Framework\TestCase;

class CountedProductTest extends TestCase
{
    public function testCount()
    {
        CountedProduct::reset();

        $p1 = new CountedProduct();
        $this->assertEquals(1, $p1->getValue());

        $p2 = new CountedProduct();
        $this->assertEquals(1, $p1->getValue());
        $this->assertEquals(2, $p2->getValue());

        CountedProduct::reset();

        $p3 = new CountedProduct();
        $this->assertEquals(1, $p1->getValue());
        $this->assertEquals(2, $p2->getValue());
        $this->assertEquals(1, $p3->getValue());

        CountedProduct::reset();
    }

}
