<?php
namespace App\Service;

use Avris\Micrus\Tool\Test\WebTestCase;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;

abstract class FunctionalTest extends WebTestCase
{
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        /** @var EntityManagerInterface $em */
        $em = static::$client->getContainer()->get(EntityManagerInterface::class);

        if ($em->getConnection()->isTransactionActive()) {
            $em->rollback();
        }

        $em->clear();
    }

    protected function login($login, $password, Crawler $crawler = null)
    {
        if (!$crawler) {
            $crawler = static::$client->request('GET', '/en_GB/login');
        }

        $form = $crawler->selectButton('Log in')->form();
        $form['Login[username]'] = $login;
        $form['Login[password]'] = $password;

        $crawler = static::$client->submit($form);

        $this->assertLoggedInAs($login);
        $this->assertEmpty($crawler->filter('ul.form-errors li'));
        $this->assertContains($login, $crawler->filter('.navbar-nav.ml-auto')->text());

        return $crawler;
    }

    protected function logout()
    {
        if (!session_id()) {
            session_start();
        }

        $crawler = static::$client->request('GET', '/en_GB/logout');
        $this->assertContains('Log in', $crawler->filter('.navbar')->text());
        $this->assertLoggedOut();

        return $crawler;
    }
}
