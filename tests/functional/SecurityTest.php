<?php
namespace App\Test;

use App\Entity\Post;
use App\Service\FunctionalTest;
use Doctrine\ORM\EntityManagerInterface;

class SecurityTest extends FunctionalTest
{
    protected static $postId;

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        static::$postId = static::$client->getContainer()->get(EntityManagerInterface::class)
            ->getRepository(Post::class)->findOneBy(['title' => 'Et id nisi qui.'])->getId(); // author: admin
    }

    public function testNotFound()
    {
        foreach (['/nonexistent', '/en_GB/nonexistent', '/en_GB/post/foo/edit'] as $route) {
            $crawler = static::$client->request('GET', $route);
            $this->assertEquals('Error 404', $crawler->filter('main h3')->text());
            $this->assertContains('Back to homepage', $crawler->filter('main a')->text());
        }
    }

    public function testRedirectToLogin()
    {
        $crawler = static::$client->request('GET', '/en_GB/post/add');

        $this->assertUrl('/en_GB/login?url=' . urlencode('/en_GB/post/add'));

        $this->assertContains('Log in', $crawler->filter('h3')->text());
    }

    public function testRegisterFail()
    {
        $crawler = static::$client->request('GET', '/en_GB/login');

        $form = $crawler->selectButton('Register')->form();
        $form['Register[username]'] = 'admin';
        $form['Register[email]'] = 'newuser@avris.it';
        $form['Register[password]'] = 'pass1';
        $form['Register[passwordRepeat]'] = 'pass2';

        $crawler = static::$client->submit($form);

        $this->assertUrl('/en_GB/login');
        $this->assertEquals(1, $crawler->filter('ul.invalid-feedback li:contains("Selected login is already taken")')->count());
        $this->assertEquals(1, $crawler->filter('ul.invalid-feedback li:contains("Given passwords don\'t match")')->count());
        $this->assertEquals(1, $crawler->filter('ul.invalid-feedback li:contains("This field is required")')->count());
    }

    public function testRegisterSuccess()
    {
        $crawler = static::$client->request('GET', '/en_GB/login');

        $form = $crawler->selectButton('Register')->form();
        $form['Register[username]'] = 'newuser';
        $form['Register[email]'] = 'newuser@avris.it';
        $form['Register[password]'] = 'passWORD';
        $form['Register[passwordRepeat]'] = 'passWORD';
        $form['Register[agree]']->tick();

        $crawler = static::$client->submit($form);

        $this->assertUrl('/en_GB/account');
        $this->assertContains('My account', $crawler->filter('h1')->text());
        $this->assertContains('newuser', $crawler->filter('h3')->text());
        $this->assertContains('newuser@avris.it', $crawler->filter('main a')->text());
        $this->assertContains('Posts count: 0', $crawler->filter('main')->text());

        $this->logout();
    }

    public function testLoginFail()
    {
        $crawler = static::$client->request('GET', '/en_GB/login');

        $form = $crawler->selectButton('Log in')->form();
        $form['Login[username]'] = 'user';
        $form['Login[password]'] = 'wrongpass';

        $crawler = static::$client->submit($form);

        $this->assertUrl('/en_GB/login');
        $this->assertEquals(1, $crawler->filter('ul.invalid-feedback li:contains("Incorrect credentials")')->count());
    }

    public function testLoginSuccess()
    {
        $crawler = static::$client->request('GET', '/en_GB/login');

        $form = $crawler->selectButton('Log in')->form();
        $form['Login[username]'] = 'user';
        $form['Login[password]'] = 'user';
        $form['Login[rememberme]']->tick();

        $crawler = static::$client->submit($form);

        $this->assertUrl('/en_GB/account');
        $this->assertContains('user', $crawler->filter('#user')->text());
        $this->assertNotEmpty(static::$client->getCookieJar()->get('rememberme')->getValue());
        $this->logout();
        $this->assertNull(static::$client->getCookieJar()->get('rememberme'));
    }

    public function testUnauthorizedEdit()
    {
        $this->login('user', 'user');

        $crawler = static::$client->request('GET', '/en_GB/post/' . static::$postId . '/edit');

        $this->assertStatus(403);
        $this->assertEquals('Error 403', $crawler->filter('main h3')->text());
        $this->assertContains('Back to homepage', $crawler->filter('main a')->text());

        $this->logout();
    }

    public function testAuthorizedEdit()
    {
        $this->login('admin', 'admin');
        $crawler = static::$client->request('GET', '/en_GB/post/' . static::$postId . '/edit');
        $this->assertSuccessful();
        $this->assertContains('General', $crawler->filter('fieldset legend')->text());
    }

    public function testAlreadyLoggedIn()
    {
        static::$client->request('GET', '/en_GB/login');
        $this->assertUrl('/en_GB/account');
    }

    public function testImpersonate()
    {
        $crawler = static::$client->request('GET', '/en_GB/account?_impersonate=newuser');
        $this->assertUrl('/en_GB/account?_impersonate=newuser');

        $this->assertContains('newuser', $crawler->filter('.navbar-nav.ml-auto')->text());
        $this->assertContains('End impersonation', $crawler->filter('.navbar-nav.ml-auto')->text());

        $link = $crawler->filter('.navbar-nav.ml-auto a:contains("End impersonation")')->link();
        $crawler = static::$client->click($link);

        $this->assertContains('admin', $crawler->filter('.navbar-nav.ml-auto')->text());
        $this->assertNotContains('End impersonation', $crawler->filter('.navbar-nav.ml-auto')->text());
    }

    public function testGrantRights()
    {
        $crawler = static::$client->request('GET', '/en_GB/admin/user/list?sort=createdAt%7Cdesc');
        $userRows = $crawler->filter('tbody tr');
        $this->assertCount(3, $userRows);
        $this->assertCount(5, $userRows->first()->filter('.crud-actions a'));
        $this->assertContains('newuser', $crawler->filter('td.crud-column-username a')->text());
        $link = $crawler->filter('a[title=Edit]')->link();
        $crawler = static::$client->click($link);

        $this->assertUrlMatches('/en_GB/admin/user/%x-%x-%x-%x-%x/edit');
        $form = $crawler->selectButton('Save')->form();
        $form['User[role]']->select('ROLE_ADMIN');
        $crawler = static::$client->submit($form);

        $this->assertUrlMatches('/en_GB/admin/user/%x-%x-%x-%x-%x/edit');
        $link = $crawler->filter('a:contains("Show")')->link();
        $crawler = static::$client->click($link);

        $this->assertContains('newuser', $crawler->filter('td.crud-field-username')->text());
        $this->assertContains('admin', $crawler->filter('td.crud-field-roleName')->text());
    }

    public function testDeleteUser()
    {
        $crawler = static::$client->request('GET', '/en_GB/admin/user/list?sort=createdAt%7Cdesc');
        $this->assertContains('newuser', $crawler->filter('td.crud-column-username a')->text());
        $link = $crawler->filter('a[title=Delete]')->link();
        $crawler = static::$client->click($link);

        $this->assertUrlMatches('/en_GB/admin/user/%x-%x-%x-%x-%x/delete');
        $this->assertContains(
            'Are you sure you want to delete this object?',
            $crawler->filter('.alert.alert-danger')->text()
        );
        $form = $crawler->selectButton('Delete')->form();
        $crawler = static::$client->submit($form);

        $this->assertUrl('/en_GB/admin/user/list');
        $this->assertContains('Object deleted successfully', $crawler->filter('.alert.alert-success')->text());
        $this->assertNotContains('newuser', $crawler->filter('table')->text());
    }

    public function testLogout()
    {
        $this->logout();
    }
}
