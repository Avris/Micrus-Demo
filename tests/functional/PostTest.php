<?php
namespace App\Test;

use App\Service\FunctionalTest;

class PostTest extends FunctionalTest
{
    protected static $postId;

    public function testCreateFail()
    {
        $crawler = static::$client->request('GET', '/en_GB/post/add');
        $crawler = $this->login('user', 'user', $crawler);
        $form = $crawler->selectButton('Save')->form();
        $crawler = static::$client->submit($form);
        $this->assertUrl('/en_GB/post/add');
        $this->assertEquals(5, $crawler->filter('ul.invalid-feedback li:contains("This field is required")')->count());
    }

    public function testCreate()
    {
        $crawler = static::$client->request('GET', '/en_GB/post/add');

        $form = $crawler->selectButton('Save')->form();
        $this->assertFalse($form->has('Post[user]'));
        $form['Post[title]'] = 'Lorem testum';
        $form['Post[content]'] = '<h3>Hey there!</h3> <p>How you doin\'?</p>';
        $categoriesIds = $form['Post[categories]']->availableOptionValues();
        $form['Post[categories]'] = [$categoriesIds[0], $categoriesIds[5]];
        $form['Post[language]'] = 'en_US';
        unset($form['Post[dependencies]']); // workaround for sf crawler parsing <script> incorrectly
        $form['Post[mainDependency][string]'] = 'str';

        $crawler = static::$client->submit($form);

        $this->assertUrlMatches('/en_GB/post/%x-%x-%x-%x-%x/read');
        $this->assertContains('Post "Lorem testum" has been saved', $crawler->filter('.alert.alert-success')->text());

        preg_match(
            '#[0-9A-Za-z]{8}-[0-9A-Za-z]{4}-[0-9A-Za-z]{4}-[0-9A-Za-z]{4}-[0-9A-Za-z]{12}#',
            static::$client->getRequest()->getUri(),
            $matches
        );
        static::$postId = $matches[0];

        $article = $crawler->filter('article');
        $this->assertEquals('Lorem testum', $article->filter('h4 a')->text());
        $this->assertContains('<h3>Hey there!</h3> <p>How you doin\'?</p>', $article->html());
        $this->assertContains('user', $article->filter('.col.text-right')->text());
        $this->assertEquals('http://placehold.it/200x200', $article->filter('img')->attr('src'));

        $categories = $article->filter('p.small a');
        $this->assertEquals('comedy', $categories->eq(0)->text());
        $this->assertEquals('tragedy', $categories->eq(1)->text());
    }

    public function testList()
    {
        $crawler = static::$client->request('GET', '/en_GB/post/list');
        $article = $crawler->filter('article')->eq(0);
        $this->assertEquals('Lorem testum', $article->filter('h4 a')->text());
        $this->assertContains('<p>Hey there! How you doin\'?...</p>', $article->html());
        $this->assertContains('user', $article->filter('.col.text-right')->text());
        $this->assertEquals('http://placehold.it/200x200', $article->filter('img')->attr('src'));
    }

    public function testEdit()
    {
        $link = static::$client->getCrawler()->filter('article')->eq(0)->filter('a:contains("Edit")')->link();
        $crawler = static::$client->click($link);

        $form = $crawler->selectButton('Save')->form();
        $form['Post[title]'] = 'Lorem testum changed';
        $form['Post[attachment]']->upload(__DIR__ . '/../_help/lorempixel.jpg');
        unset($form['Post[dependencies]']);

        $crawler = static::$client->submit($form);

        $this->assertUrlMatches('/en_GB/post/%x-%x-%x-%x-%x/read');
        $this->assertContains(
            'Post "Lorem testum changed" has been saved',
            $crawler->filter('.alert.alert-success')->text()
        );

        $article = $crawler->filter('article');
        $this->assertEquals('Lorem testum changed', $article->filter('h4 a')->text());
        $this->assertContains('<h3>Hey there!</h3> <p>How you doin\'?</p>', $article->html());
        $this->assertContains('user', $article->filter('.col.text-right')->text());
        $this->assertRegExp('#.*imagine/thumb/[A-Za-z0-9]{12}\.jpg#', $article->filter('img')->attr('src'));

        $categories = $article->filter('p.small a');
        $this->assertEquals('comedy', $categories->eq(0)->text());
        $this->assertEquals('tragedy', $categories->eq(1)->text());
    }

    public function testCategory()
    {
        $link = static::$client->getCrawler()->filter('article')->eq(0)->filter('p.small a')->link();
        $crawler = static::$client->click($link);

        $this->assertUrl('/en_GB/post/category/comedy');
        $this->assertContains(
            'Lorem testum changed',
            $crawler->filter('article#' . static::$postId)->filter('h4 a')->text()
        );

        $crawler = static::$client->request('GET', '/en_GB/post/category/drama');
        $this->assertNotContains('Lorem testum changed', $crawler->text());
    }

    public function testEditRights()
    {
        $articleUser = static::$client->getCrawler()->filter('article')->eq(3);
        $this->assertContains('Dolores dolorum amet iste.', $articleUser->filter('h4 a')->text());
        $aside = $articleUser->filter('.col.text-right')->text();
        $this->assertNotContains('admin', $aside);
        $this->assertContains('user', $aside);
        $this->assertContains('Edit', $aside);
        $this->assertContains('Delete', $aside);

        $articleAdmin = static::$client->getCrawler()->filter('article')->eq(2);
        $this->assertContains('Et id nisi qui.', $articleAdmin->filter('h4 a')->text());
        $aside = $articleAdmin->filter('.col.text-right')->text();
        $this->assertContains('admin', $aside);
        $this->assertNotContains('user', $aside);
        $this->assertNotContains('Edit', $aside);
        $this->assertNotContains('Delete', $aside);
    }

    public function testEditAdmin()
    {
        $this->logout();
        $this->login('admin', 'admin');

        $crawler = static::$client->request('GET', '/en_GB/post/list');
        $link = $crawler->filter('article')->eq('0')->filter('.col.text-right a:contains("Edit")')->link();
        $crawler = static::$client->click($link);

        $form = $crawler->selectButton('Save')->form();
        $this->assertTrue($form->has('Post[user]'));
        $this->assertRegExp(
            '#.*imagine/thumb/[A-Za-z0-9]{12}\.jpg#',
            $crawler->filter('.form-group img')->attr('src')
        );
    }

    public function testDelete()
    {
        $crawler = static::$client->request('GET', '/en_GB/post/list');
        $link = $crawler->filter('article#' . static::$postId)->filter('.col.text-right a:contains("Delete")')->link();
        $crawler = static::$client->click($link);

        $this->assertContains(
            'Are you sure you want to delete the post "Lorem testum changed"?',
            $crawler->filter('form.well')->text()
        );

        $form = $crawler->selectButton('Delete')->form();
        $crawler = self::$client->submit($form);

        $this->assertUrl('/en_GB/post/list');
        $this->assertContains(
            'Post "Lorem testum changed" has been deleted',
            $crawler->filter('.alert.alert-success')->text()
        );

        $this->assertCount(0, $crawler->filter('article:contains("Lorem testum changed")'));
    }

    public function testCleanup()
    {
        $this->logout();
    }
}
