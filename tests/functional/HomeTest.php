<?php
namespace App\Test;

use App\Service\CountedProduct;
use App\Service\FunctionalTest;
use Symfony\Component\BrowserKit\Response;

class HomeTest extends FunctionalTest
{
    public function testHomepageWithLanguages()
    {
        $crawler = static::$client->request('GET', '/', [], [], [
            'HTTP_ACCEPT_LANGUAGE' => 'en-GB,en-US;q=0.9,de;q=0.8',
        ]);

        $this->assertSuccessful();
        $this->assertUrl('/en_GB');
        $this->assertContains('Micrus', $crawler->filter('.navbar .navbar-brand')->text());

        $this->assertContains('Beauty of simplicity', $crawler->filter('.jumbotron .h2')->text());

        $crawler = static::$client->click($crawler->filter('a:contains("Polski")')->link());
        $this->assertUrl('/pl');
        $this->assertContains('Piękno prostoty', $crawler->filter('.jumbotron .h2')->text());

        static::$client->click($crawler->filter('a:contains("English (GB)")')->link());
    }

    public function testExtensions()
    {
        $crawler = static::$client->request('GET', '/en_GB/extensions');
        $main = $crawler->filter('main')->text();
        $this->assertContains('tEsT TwIg eXtEnSiOn', $main);
        $this->assertContains(date('d/m/Y'), $main);
    }

    public function testFlash()
    {
        $crawler = static::$client->request('GET', '/en_GB/flash');

        $form = $crawler->selectButton('OK')->form();
        $crawler = static::$client->submit($form);
        $this->assertContains(
            'This field is required',
            $crawler->filter('ul.invalid-feedback li')->text()
        );

        $form = $crawler->selectButton('OK')->form();
        $form['form[message]'] = 'Success message';
        $crawler = static::$client->submit($form);
        $this->assertContains(
            'Success message',
            $crawler->filter('.alert.alert-success')->text()
        );
        $this->assertEmpty($crawler->filter('.alert.alert-danger'));

        $form = $crawler->selectButton('OK')->form();
        $form['form[type]']->select('danger');
        $form['form[message]'] = 'Danger message';
        $crawler = static::$client->submit($form);
        $this->assertContains(
            'Danger message',
            $crawler->filter('.alert.alert-danger')->text()
        );
        $this->assertEmpty($crawler->filter('.alert.alert-success'));
    }

    public function testMail()
    {
        $crawler = static::$client->request('GET', '/en_GB/mail');

        $form = $crawler->selectButton('Send')->form();
        $form['form[email]'] = 'test@example.com';
        $crawler = static::$client->submit($form);

        $this->assertContains(
            'Mail has been spooled for sending to test@example.com',
            $crawler->filter('.alert.alert-success')->text()
        );

        $this->assertContains(
            'bin/micrus mailer:spool:send',
            $crawler->filter('.alert.alert-success')->text()
        );

        $this->assertContains(
            'Hello, Anonymous User!',
            $crawler->filter('div.main')->text()
        );
    }

    public function testFactory()
    {
        static::$client->request('GET', '/en_GB/factory');
        /** @var Response $response */
        $response = static::$client->getResponse();

        $this->assertSuccessful();
        $this->assertContentTypeJson();

        $this->assertEquals(
            '{"1":1,"2":1,"3":1,"4":2,"5":3,"6":4,"7":1,"8":1,"9":1}',
            $response->getContent()
        );

        CountedProduct::reset();
    }

    public function testCookies()
    {
        $crawler = static::$client->request('GET', '/en_GB/cookies');
        $this->assertEquals('0', $crawler->filter('.cookie-count')->text());

        $crawler = static::$client->request('GET', '/en_GB/cookies');
        $this->assertEquals('1', $crawler->filter('.cookie-count')->text());

        $crawler = static::$client->request('GET', '/en_GB/cookies');
        $this->assertEquals('2', $crawler->filter('.cookie-count')->text());
    }

    public function testRequestListener()
    {
        static::$client->request('GET', '/en_GB?json=true');
        /** @var Response $response */
        $response = static::$client->getResponse();

        $this->assertSuccessful();
        $this->assertContentTypeJson();

        $this->assertEquals(
            '{"get":"true"}',
            $response->getContent()
        );
    }

    public function testFiles()
    {
        $crawler = static::$client->request('GET', '/en_GB/files');
        $this->assertSuccessful();

        static::$client->click($crawler->filter('a:contains("Open logo")')->link());
        /** @var Response $response */
        $response = static::$client->getResponse();
        $this->assertContentType('image/png');
        $this->assertArrayNotHasKey('Content-Disposition', $response->getHeaders());

        $crawler = static::$client->back();
        static::$client->click($crawler->filter('a:contains("Download logo")')->link());
        /** @var Response $response */
        $response = static::$client->getResponse();
        $this->assertContentType('image/png');
        $this->assertArrayHasKey('Content-Disposition', $response->getHeaders());
        $this->assertEquals('attachment; filename="logo.png"', $response->getHeaders()['Content-Disposition']);

        $crawler = static::$client->back();
        static::$client->click($crawler->filter('a:contains("Fetch from the internet")')->link());
        $this->assertContentType('image/png');
    }
}
