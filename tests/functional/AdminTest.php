<?php
namespace App\Test;

use App\Entity\User;
use App\Service\FunctionalTest;
use Doctrine\ORM\EntityManagerInterface;

class AdminTest extends FunctionalTest
{
    public function testAccess()
    {
        $crawler = static::$client->request('GET', '/en_GB/admin/post/list');
        $this->assertUrl('/en_GB/login?url=' . urlencode('/en_GB/admin/post/list'));
        $this->login('user', 'user', $crawler);
        $this->assertUrl('/en_GB/admin/post/list');
        $this->assertStatus(403);
        $this->logout();

        $crawler = static::$client->request('GET', '/en_GB/admin/post/list');
        $this->assertUrl('/en_GB/login?url=' . urlencode('/en_GB/admin/post/list'));
        $crawler = $this->login('admin', 'admin', $crawler);
        $this->assertSuccessful();
        $this->assertUrl('/en_GB/admin/post/list');
        $this->assertContains('Dashboard', $crawler->filter('.navbar-nav')->text());

        $link = $crawler->filter('a:contains("Polski")')->link();
        $crawler = static::$client->click($link);
        $this->assertUrl('/pl/admin/post/list');
        $this->assertContains('Panel administracyjny', $crawler->filter('.navbar-nav')->text());
    }

    public function testDashboard()
    {
        $link = static::$client->getCrawler()->filter('.navbar-nav a:contains("Panel administracyjny")')->link();
        $crawler = static::$client->click($link);

        $this->assertCount(3, $crawler->filter('.card'));

        // Posts
        $header = $crawler->filter('.dashboard-card-Post .card-header')->text();
        $this->assertContains('Posty', $header);
        $this->assertContains('Nowy', $header);
        $this->assertContains('Lista', $header);

        $metric = $crawler->filter('.dashboard-card-Post .list-group-item')->eq(0)->text();
        $this->assertContains('12', $metric);
        $this->assertContains('Wszystkie', $metric);

        $metric = $crawler->filter('.dashboard-card-Post .list-group-item')->eq(1)->text();
        $this->assertContains('W zeszłym tygodniu', $metric);

        // Users
        $header = $crawler->filter('.dashboard-card-User .card-header')->text();
        $this->assertContains('Użytkownicy', $header);
        $this->assertNotContains('Nowy', $header);
        $this->assertContains('Lista', $header);

        $metric = $crawler->filter('.dashboard-card-User .list-group-item')->eq(0)->text();
        $this->assertContains('2', $metric);
        $this->assertContains('Wszyscy', $metric);

        $metric = $crawler->filter('.dashboard-card-User .list-group-item')->eq(1)->text();
        $this->assertContains('2', $metric);
        $this->assertContains('W zeszłym tygodniu', $metric);

        $metric = $crawler->filter('.dashboard-card-User .list-group-item')->eq(2)->text();
        $this->assertContains('6', $metric);
        $this->assertContains('Średnio postów na użytkownika', $metric);

        // Categories
        $header = $crawler->filter('.dashboard-card-Category .card-header')->text();
        $this->assertContains('Kategorie', $header);
        $this->assertContains('Nowy', $header);
        $this->assertContains('Lista', $header);

        $metric = $crawler->filter('.dashboard-card-Category .list-group-item')->eq(0)->text();
        $this->assertContains('6', $metric);
        $this->assertContains('Wszystkie', $metric);
    }

    public function testList()
    {
        $crawler = static::$client->request('GET', '/pl/admin/post/list');
        $this->assertCount(5, $crawler->filter('tbody tr'));
        $this->assertCount(3, $crawler->filter('.crud-pagination a'));
        $this->assertContains('12 elementów', $crawler->filter('tfoot .float-right')->text());

        $form = $crawler->selectButton('Przefiltruj')->form();
        $form['Post_filter[user]']->select($this->getUserId('admin'));
        $crawler = static::$client->submit($form);

        $link = $crawler->filter('th.crud-column-title a.crud-sort-desc')->link();
        $crawler = static::$client->click($link);

        $link = $crawler->filter('.crud-pagination a:contains("2")')->link();
        $crawler = static::$client->click($link);

        $this->assertUrl('/pl/admin/post/list?page=2&sort=title%7Cdesc');
        $entries = $crawler->filter('tbody tr');
        $this->assertCount(1, $entries);
        $this->assertContains('Dignissimos perferendis voluptatibus incidunt.', $entries->text());
        $this->assertContains('admin', $entries->text());
        $this->assertContains('comedy', $entries->text());
        $this->assertContains('info', $entries->text());
        $this->assertContains('British English', $entries->text());

        $link = $crawler->filter('.crud-filter-clear')->link();
        $crawler = static::$client->click($link);
        $this->assertUrl('/pl/admin/post/list?sort=title%7Cdesc&clear=1');
        $this->assertCount(5, $crawler->filter('tbody tr'));
    }

    protected function getUserId(string $identifier)
    {
        return static::$client
            ->getContainer()
            ->get(EntityManagerInterface::class)
            ->getRepository(User::class)
            ->findOneBy(['username' => $identifier])
            ->getId();
    }

    public function testPostLifecycle()
    {
        $crawler = static::$client->request('GET', '/pl/admin/post/list');
        $this->assertCount(5, $crawler->filter('tbody tr'));
        $this->assertCount(3, $crawler->filter('tbody tr')->first()->filter('.crud-actions a'));
        $this->assertCount(3, $crawler->filter('.crud-pagination a'));
        $link = $crawler->filter('a:contains("Nowy")')->link();
        $crawler = static::$client->click($link);

        $this->assertUrl('/pl/admin/post/new');
        $this->assertContains('Nowy', $crawler->filter('.breadcrumb')->text());
        $form = $crawler->selectButton('Zapisz')->form();
        $form['Post[title]'] = 'Test title';
        $form['Post[content]'] = '<p>Test content</p>';
        $categoriesIds = $form['Post[categories]']->availableOptionValues();
        $form['Post[categories]'] = [$categoriesIds[2], $categoriesIds[4]];
        $form['Post[language]']->select('en_US');
        unset($form['Post[dependencies]']); // workaround for sf crawler parsing <script> incorrectly
        $form['Post[mainDependency][string]'] = 'str';
        $crawler = static::$client->submit($form);

        $this->assertUrlMatches('/pl/admin/post/%x-%x-%x-%x-%x/edit');
        $this->assertContains('Test title', $crawler->filter('.breadcrumb')->text());
        $this->assertContains('Edytuj', $crawler->filter('.breadcrumb')->text());
        $form = $crawler->selectButton('Zapisz')->form();
        $form['Post[title]'] = 'New title';
        unset($form['Post[dependencies]']); // workaround for sf crawler parsing <script> incorrectly
        $crawler = static::$client->submit($form);

        $link = $crawler->filter('a:contains("Pokaż")')->link();
        $crawler = static::$client->click($link);
        $this->assertContains('New title', $crawler->filter('.breadcrumb')->text());
        $this->assertContains('Tytuł', $crawler->filter('th.crud-field-title')->text());
        $this->assertContains('New title', $crawler->filter('td.crud-field-title')->text());
        $this->assertContains('Treść', $crawler->filter('th.crud-field-content')->text());
        $this->assertContains('Test content', $crawler->filter('td.crud-field-content')->text());
        $this->assertContains('Autor', $crawler->filter('th.crud-field-user')->text());
        $this->assertContains('admin', $crawler->filter('td.crud-field-user')->text());
        $this->assertContains('Kategorie', $crawler->filter('th.crud-field-categories')->text());
        $this->assertCount(2, $crawler->filter('td.crud-field-categories a'));
        $this->assertContains('drama', $crawler->filter('td.crud-field-categories')->text());
        $this->assertContains('news', $crawler->filter('td.crud-field-categories')->text());
        $this->assertStringMatchesFormat(
            '/pl/admin/category/%x-%x-%x-%x-%x/show',
            $crawler->filter('td.crud-field-categories a')->first()->attr('href')
        );

        $link = $crawler->filter('a:contains("Przeczytaj")')->link();
        $crawler = static::$client->click($link);
        $this->assertUrlMatches('/pl/post/%x-%x-%x-%x-%x/read');
        $this->assertContains('New title', $crawler->filter('h4 a')->text());
        $this->assertContains('Test content', $crawler->filter('article')->text());

        $crawler = static::$client->request('GET', '/pl/admin/post/list');
        $form = $crawler->selectButton('Przefiltruj')->form();
        $form['Post_filter[title]'] = 'New title';
        $crawler = static::$client->submit($form);

        $link = $crawler->filter('tbody tr')->first()->filter('.crud-actions a[title=Usuń]')->link();
        $crawler = static::$client->click($link);
        $this->assertUrlMatches('/pl/admin/post/%x-%x-%x-%x-%x/delete');
        $this->assertContains('New title', $crawler->filter('.breadcrumb')->text());
        $this->assertContains('Usuń', $crawler->filter('.breadcrumb')->text());
        $this->assertContains(
            'Czy na pewno chcesz usunąć ten obiekt?',
            $crawler->filter('.alert.alert-danger')->text()
        );
        $form = $crawler->selectButton('Usuń')->form();
        $crawler = static::$client->submit($form);

        $this->assertUrl('/pl/admin/post/list');
        $this->assertContains('Obiekt został usunięty', $crawler->filter('.alert.alert-success')->text());
        $this->assertNotContains('new title', $crawler->filter('table')->text());
    }

    public function testCategoryLifecycle()
    {
        $crawler = static::$client->request('GET', '/pl/admin/category/list');
        $this->assertCount(6, $crawler->filter('tbody tr'));
        $this->assertCount(0, $crawler->filter('.crud-pagination a'));
        $link = $crawler->filter('a:contains("Nowy")')->link();
        $crawler = static::$client->click($link);

        $this->assertUrl('/pl/admin/category/new');
        $this->assertContains('Kategorie', $crawler->filter('.breadcrumb')->text());
        $this->assertContains('Nowy', $crawler->filter('.breadcrumb')->text());
        $form = $crawler->selectButton('Zapisz i dodaj kolejny')->form();
        $form['Category[name]'] = 'Nowa';
        $crawler = static::$client->submit($form);

        $this->assertUrlMatches('/pl/admin/category/new?id=%x-%x-%x-%x-%x');
        $this->assertContains('Kategorie', $crawler->filter('.breadcrumb')->text());
        $this->assertContains('Nowy', $crawler->filter('.breadcrumb')->text());

        $crawler = static::$client->request('GET', '/pl/admin/category/list');
        $form = $crawler->selectButton('Przefiltruj')->form();
        $form['Category_filter[name]'] = 'Nowa';
        $crawler = static::$client->submit($form);

        $link = $crawler->filter('tbody tr')->first()->filter('.crud-actions a[title=Edytuj]')->link();
        $crawler = static::$client->click($link);
        $form = $crawler->selectButton('Zapisz')->form();
        $form['Category[name]'] = 'Jeszcze nowsza';
        $crawler = static::$client->submit($form);

        $link = $crawler->filter('a:contains("Pokaż")')->link();
        $crawler = static::$client->click($link);
        $this->assertContains('Nazwa', $crawler->filter('th.crud-field-name')->text());
        $this->assertContains('Jeszcze nowsza', $crawler->filter('td.crud-field-name')->text());
        $this->assertContains('Posty', $crawler->filter('th.crud-field-posts')->text());
        $this->assertCount(0, $crawler->filter('td.crud-field-categories a'));

        $crawler = static::$client->request('GET', '/pl/admin/category/list');
        $form = $crawler->selectButton('Przefiltruj')->form();
        $form['Category_filter[name]'] = 'Jeszcze nowsza';
        $crawler = static::$client->submit($form);

        $link = $crawler->filter('tbody tr')->first()->filter('.crud-actions a[title=Usuń]')->link();
        $crawler = static::$client->click($link);
        $this->assertUrlMatches('/pl/admin/category/%x-%x-%x-%x-%x/delete');
        $this->assertContains(
            'Czy na pewno chcesz usunąć ten obiekt?',
            $crawler->filter('.alert.alert-danger')->text()
        );
        $form = $crawler->selectButton('Usuń')->form();
        $crawler = static::$client->submit($form);

        $this->assertUrl('/pl/admin/category/list');
        $this->assertContains('Obiekt został usunięty', $crawler->filter('.alert.alert-success')->text());
        $this->assertNotContains('Jeszcze nowsza', $crawler->filter('table')->text());
    }

    public function testCustomListAction()
    {
        $crawler = static::$client->request('GET', '/pl/admin/user/list');
        $link = $crawler->filter('a:contains("Testowy email")');
        $this->assertStringMatchesFormat('/pl/admin/user/%x-%x-%x-%x-%x/email', $link->attr('href'));

        $crawler = static::$client->request('GET', '/en_GB/admin/user/'.$this->getUserId('user').'/email');

        $this->assertContains(
            'Mail has been spooled for sending to user@micrus.avris.it',
            $crawler->filter('.alert.alert-success')->text()
        );
        $this->assertUrl('/en_GB/admin/user/list');
    }

    public function testLogout()
    {
        $crawler = $this->logout();

        $this->assertNotContains('Panel administracyjny', $crawler->filter('.navbar-nav')->text());
        $this->assertNotContains('Admin Panel', $crawler->filter('.navbar-nav')->text());
        $this->assertContains('Other', $crawler->filter('.navbar-nav')->text());
    }
}
