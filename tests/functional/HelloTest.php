<?php
namespace App\Test;

use App\Service\FunctionalTest;

class HelloTest extends FunctionalTest
{
    public function testHelloYou()
    {
        $crawler = static::$client->request('GET', '/en_GB');
        $this->assertNotContains('Hey, you!', $crawler->filter('.navbar')->text());

        $this->assertUrl('/en_GB');

        $crawler = $this->login('admin', 'admin');
        $link = $crawler->filter('.navbar a:contains("Hey, you!")')->link();
        $crawler = static::$client->click($link);

        $this->assertUrlMatches('/en_GB/hello/%x-%x-%x-%x-%x');
        $this->assertContains('Hello, admin!', $crawler->filter('main')->text());

        $this->logout();
    }

    public function testHelloName()
    {
        $link = static::$client->getCrawler()->filter('.navbar a:contains("Hi there, Andre!")')->link();
        $crawler = static::$client->click($link);

        $this->assertUrl('/en_GB/hello/Andre');
        $this->assertContains('Hello, Andre!', $crawler->filter('main')->text());

        $crawler = static::$client->request('GET', '/en_GB/hello/Tom');
        $this->assertContains('Hello, Tom!', $crawler->filter('main')->text());
    }

    public function testHelloStranger()
    {
        $link = static::$client->getCrawler()->filter('.navbar a:contains("Hello, Stranger!")')->link();
        $crawler = static::$client->click($link);

        $this->assertUrl('/en_GB/hello');
        $this->assertContains('Hello, Stranger!', $crawler->filter('main')->text());
    }

    public function testHelloLanguageSwitch()
    {
        $crawler = static::$client->click(static::$client->getCrawler()->filter('a:contains("Polski")')->link());
        $this->assertUrl('/pl/hello');
        $this->assertContains('Witaj, Nieznajomy!', $crawler->filter('main')->text());
    }
}
