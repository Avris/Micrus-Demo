<?php
require_once __DIR__ . '/../vendor/autoload.php';

foreach (glob(__DIR__ . '/_help/*.php') as $file) {
    require_once $file;
}

(new \Avris\Dotenv\Dotenv)->load(__DIR__ . '/.env');

session_start();

date_default_timezone_set('Europe/Berlin');

$app = new \App\App(getenv('APP_ENV'), getenv('APP_DEBUG'));

$app->warmup()->get(\Avris\Dispatcher\EventDispatcherInterface::class)
    ->trigger(new \Avris\Micrus\Tool\Cache\CacheClearEvent());

$consoleApp = new \Avris\Micrus\Console\ConsoleApplication($app);
$consoleApp->setAutoExit(false);
$consoleApp->run(new \Symfony\Component\Console\Input\ArgvInput(['', 'db:schema:create']));
$consoleApp->run(new \Symfony\Component\Console\Input\ArgvInput(['', 'db:fixtures', '--dont-truncate', '--force', '--local-image']));

\Avris\Micrus\Tool\Test\WebTestCase::init($app, __DIR__ . '/../var/tests/fails');
